module Main where

import Graphics.Gloss
import Graphics.Gloss.Interface.IO.Interact

import System.Random
import Data.List
import Control.Monad.State

import Game.Core
import Game.Draw

foodGrowth = 5 -- How much the snake grows when eating food
fps = 10 -- framerate for the game

initWorld :: [Position] -> World
initWorld (foodPosition:foodPositions) = World { snake = SS ([(0, 0)], East)
                                               , inputDirection = East
                                               , foodPosition = foodPosition
                                               , foodPositions = foodPositions
                                               , growth = 0
                                               }
initWorld [] = undefined

-- World is 11 by 11
worldSize :: Int
worldSize = 11

randomCell = runState ((,) <$> randomCoord <*> randomCoord)
  where randomCoord = state $ uniformR (0, worldSize - 1)

main :: IO ()
main = do
  stdGen <- initStdGen
  let foodPositions = unfoldr (Just . randomCell) stdGen
  play window white fps (Alive, initWorld foodPositions) picture handleEvent' stepGame

picture :: (GameState, World) -> Picture
picture (s, w) = pictures $ [ drawBackground worldSize
                            , drawSnakeHead $ getHead $ snake w
                            , drawFood $ foodPosition w ] ++ snakeTail ++ [gameOverMessage]
                                where gameOverMessage = case s of
                                                          Alive -> blank
                                                          Dead -> scale 0.4 0.4 $ text "Game over"
                                      snakeTail = map drawSnakeHead $ getTail $ snake w

keyMap :: [(SpecialKey, Direction)]
keyMap = [ (KeyRight, East)
         , (KeyLeft, West)
         , (KeyUp, North)
         , (KeyDown, South)
         ]

handleEvent :: Event -> World -> World
handleEvent (EventKey (SpecialKey k) Down _ _) w
  = case lookup k keyMap of
      Just d -> if canMove d (lastMoveDir $ snake w)
                   then w { inputDirection = d }
                   else w
        where canMove d1 d2 = not $ is180DegreeTurn d1 d2
              is180DegreeTurn d1 d2 = (abs (fromEnum d1 - fromEnum d2)) == 2
      Nothing -> w
handleEvent _ w = w

handleEvent' e (Alive, w) = (Alive, handleEvent e w)
handleEvent' e s = s

stepGame _ (Alive, w)
  = let (nextGrowth', nextPos) = case growth w of
                                   0 -> (0, step (inputDirection w) (snake w))
                                   n -> (n-1, stepGrow (inputDirection w) (snake w))
        nextHeadPos = getHead nextPos
        nextTailPos = getTail nextPos
        collides = collidesWithWall || collidesWithSelf
        collidesWithWall = isOutOfBounds worldSize nextPos
        collidesWithSelf = nextHeadPos `elem` nextTailPos
        collidesWithFood = nextHeadPos == (foodPosition w)
        isOutOfBounds _ _ = False
     in if collides
           then (Dead, w)
           else (Alive, if collidesWithFood
             then let (nextFoodPos, restFoodPos) = selectFoodPos (getSnake nextPos) (foodPositions w)
                      selectFoodPos s (f:fs) | f `elem` s = let (n, r) = selectFoodPos s fs
                                                             in (n, f:r)
                                             | otherwise = (f, fs)
                      in w { snake = nextPos
                           , foodPosition = nextFoodPos
                           , foodPositions = restFoodPos
                           , growth = nextGrowth' + foodGrowth
                           }
             else w { snake = nextPos, growth = nextGrowth' })
stepGame _ s = s

window :: Display
window = InWindow "Nice Window" (200, 200) (10, 10)
