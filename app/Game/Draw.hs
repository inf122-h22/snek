module Game.Draw where

import Graphics.Gloss

import Game.Core

drawBackground :: Int -> Picture
drawBackground size = color (greyN 0.5) $ rectangleSolid s s
  where s = fromIntegral size * cellSize

drawSnakeHead :: Position -> Picture
drawSnakeHead (x, y) = color blue $ move x y $ drawCell

drawFood :: Position -> Picture
drawFood (x, y) = color red $ move x y $ drawCell

drawCell :: Picture
drawCell = rectangleSolid cellSize cellSize

cellSize :: Float
cellSize = 20

move :: Int -> Int -> Picture -> Picture
move x y = translate (fromIntegral x * cellSize) (fromIntegral y * cellSize)


