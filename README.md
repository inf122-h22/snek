Very basic skeleton for a snake game in Haskell using the gloss library.

Run using `cabal run`.

![Game screenshot](img/screenshot.png)
