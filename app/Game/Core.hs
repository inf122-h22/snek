module Game.Core where

data Direction = North
               | West
               | South
               | East
               deriving Enum

type Position = (Int, Int)

data GameState = Dead
               | Alive

class Snake s where
  -- Move the snake a single step in a given direction, the tail will follow.
  step :: Direction -> s -> s
  -- Same as before, but grow a step in the given direction (so the end of the
  -- tail stays put).
  stepGrow :: Direction -> s -> s
  -- Return the location of the snake's head.
  getHead :: s -> Position
  -- Return the snake's tail.
  getTail :: s -> [Position]
  -- Return the entire snake (head + tail).
  getSnake :: s -> [Position]
  -- Return the direction of the last move (used here because whenever the player
  -- turns, we need to know if the turn is valid -- is it within 90 degree of
  -- the previous move?)
  lastMoveDir :: s -> Direction

newtype SimpleSnake = SS ([Position], Direction)

unSS (SS s) = s

instance Snake SimpleSnake where
  step dir (SS (snek@(hed:_), _)) = SS (movePosition dir hed : init snek, dir)
  stepGrow dir (SS (snek@(hed:_), _)) = SS (movePosition dir hed : snek, dir)
  getHead = head . fst . unSS
  getTail = tail . fst . unSS
  getSnake = fst . unSS
  lastMoveDir = snd . unSS

data World = World { snake :: SimpleSnake
                   , inputDirection :: Direction -- The current direction the
                   -- player wants the snake to move (i.e. the previous
                   -- directional input received from the player [unless that
                   -- input was invalid]).
                   , foodPosition :: Position -- Current food position.
                   , foodPositions :: [Position] -- Future food positions.
                   , growth :: Integer -- For many of the coming steps the snake
                   -- should grow. E.g. growth=5 will make the snake grow in
                   -- length during the next 5 steps.
                   }

movePosition :: Direction -> Position -> Position
movePosition East (x, y) = (x + 1, y)
movePosition West (x, y) = (x - 1, y)
movePosition North (x, y) = (x, y + 1)
movePosition South (x, y) = (x, y - 1)

